

<!DOCTYPE html>

<html>

    <head>

	<title> Do You Like Rock Music? Fan Page</title>

    </head>

    <body>

	<table border="1" width="100%">
		
		<tr>
			<td>

			<p align="center">

			<img src="http://img2-ak.lst.fm/i/u/300x300/fd58166e10c44b35cb988e74e1f27d6b.jpg"
			alt="Album Cover for Do You Like Rock Music?" style="width:300px;height300px">
		
			</p>

			<h2 align="center">Do You Like Rock Music?</h2>

			<a href="http://www.britishseapower.co.uk/" target="_blank">
				
			<h3 align="center">British Sea Power</h3>

			</a>

			<p align="center">January 14, 2008</p>

			</td>

		</tr>
		
		

		<tr>
			
			<td>

			<h3 align="center">Band Members and Producers</h3>

			
		
		<ul>

			<li>Jan Scott Wilkinson <i>-vocals, guitar</i></li>	
			
			<li>Martin Noble <i>-guitar</i></li>

			<li>Neil Hamilton Wilkinson <i>-bass, vocals, guitar</i></li>

			<li>Mathew Wood <i>-drums</i></li>

			<li>Graham Sutton <i>-producer</i></li>

		</ul>
		
		

		<h3 align="center">Tracks</h3>
		
			<table align="center" border="1" width="60%">

			<tr bgcolor="grey">

				<th>Track #</th>

				<th>Track Title</th>

				<th>Track Length</th>

			</tr>

			<tr align="center">
				<td>1</td>
			
				<td>"All in It"</td>

				<td>2:11</td>

			</tr>

			<tr align="center">
				<td>2</td>
			
				<td>"Lights Out for Darker Skies"</td>

				<td>6:36</td>

			</tr>

			<tr align="center">
				<td>3</td>
			
				<td>"No Lucifer"</td>

				<td>3:27</td>

			</tr>

			<tr align="center">
				<td>4</td>
			
				<td>"Waving Flags"</td>

				<td>4:07</td>

			</tr>

			<tr align="center">
				<td>5</td>
			
				<td>"Canvey Island"</td>

				<td>3:41</td>

			</tr>

			<tr align="center">
				<td>6</td>
			
				<td>"Down on the Ground"</td>

				<td>4:23</td>

			</tr>

			<tr align="center">
				<td>7</td>
			
				<td>"A Trip Out"</td>

				<td>3:16</td>

			</tr>

			<tr align="center">
				<td>8</td>
			
				<td>"The Great Skua"</td>

				<td>4:35</td>

			</tr>

			<tr align="center">
				<td>9</td>
			
				<td>"Atom"</td>

				<td>5:38</td>

			</tr>

			<tr align="center">
				<td>10</td>
			
				<td>"No Need to Cry"</td>

				<td>3:43</td>

			</tr>

			<tr align="center">
				<td>11</td>
			
				<td>"Open the Door"</td>

				<td>4:56</td>

			</tr>

			<tr align="center">
				<td>12</td>
			
				<td>"We Close Our Eyes"</td>

				<td>8:04</td>

			</tr>

			
			
			</table>
		
	
		</td>
		</tr>		


	</table>
	
    </body>

</html>

